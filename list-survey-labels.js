/**
 * Traverse a SurveyMonkey survey definition JSON file and
 * generate a list of labels survey question and answer with
 * corresponding id.
 * 
 * The resulting CSV spreadsheet can be used as a base to edit/translate
 * labels to be processed by the update-survey-labels.js script.
 * 
 * Syntax:
 * 
 * node list-survey-labels.js --survey-data=<JSON_data_file.json> --language-code=<language-code>
 * 
 * A CSV export of the labels is sent to stdout.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with this program. If not, see
 * <http://www.gnu.org/licenses/>. 
 */

var traverse = require('traverse');
var fs = require('fs');
var util = require('util');
var loader = require('csv-load-sync');
var fast_csv = require('fast-csv');

var csv, questions, answers, data, language_code;
var argv = require('minimist')(process.argv.slice(2));
var labels = [];

/**
 * Parse CLI arguments and emit errors/warnings as applicable if needed
 */
if(!argv['survey-data']) {
  process.stderr.write("Please provide a survey data file (--survey-data=<file.json>).\n");
  process.exit(1);
}

if(!argv['language-code']) {
  process.stderr.write("No language code provided (e.g. --language-code=<en_GB>): defaulting to en_GB.\n");
  language_code = 'en_GB';
}

// Load survey data from JSON file
data = JSON.parse(fs.readFileSync(argv['survey-data'], 'utf8'));

language_code = language_code || argv['language-code'];

data.forEach(extract_questions);

function extract_questions(item) {
  var question = {};
  
  if(item.code) {
    item.questions.forEach(extract_questions);
  } else if(item.question_id) {
    question['question_id'] = item.question_id;
    question['answer_id'] = null;
    question['label_' + language_code] = item.heading;
    
    labels.push(question);
    
    item.answers.forEach(extract_questions);
  } else if(item.answer_id) {
    question['question_id'] = null;
    question['answer_id'] = item.answer_id;
    question['label_' + language_code] = item.text;
    
    labels.push(question);
  }
}

fast_csv.write(labels, { headers: true }).pipe(process.stdout);
