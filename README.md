# New Urban Governance - Survey microsite data

This repository contains data processing scripts for the New Urban
Governance survey microsite.

# Updating survey question and answer labels

Questions and answers for the survey (as it has been run in 2014) are
available in the file `survey-data.json`; this has been exported via
the SurveyMonkey API and is used in the survey microsite app.

In order to update any labels for questions and answers, or to add
translations of any question or answer labels to further languages,
a simple spreadsheet file listing question/answer IDs and updated
labels/translations can be used. We then process this through a script
that updates the JSON file, producing a new JSON file for each language.

The spreadsheet used to compile updated labels and translations must
follow this layout:

| question_id | answer_id  | label_en_GB      | label_es_ES                | label_fr_FR               |
|-------------|------------|------------------|----------------------------|---------------------------|
| 673777758   |            | Is your city...  | [Spanish translation here] | [French translation here] |
|             | 7764599977 | No               | [Spanish translation here] | [French translation here] |

And so on.

Please note:

1. For each question listed in the original JSON file, add its
   question_id (normally a 9-digit number) in the spreadsheet's
   question_id column, and its label (this is listed as "heading" in the
   JSON file) in the label_en_GB field. Translations of each question
   must be added to the relevant language column.
2. For each answer listed in the original JSON file, add its answer_id
   (normally a 9-digit number) in the spreadsheet's answer_id column,
   and its label (this is listed as "text" in the JSON file) in the
   label_en_GB field. Translations of each answer must be added to
   the relevant language column.
3. In practice, each spreadsheet row must contain **either** a
   question_id **or** an answer_id and any relevant labels.
4. If a label for a question or answer doesn't need to be updated and no
   translation is needed, there is no need to list it as a row of
   the spreadsheet. For any label omitted from the labels file, the
   original label will be used.
5. For translations columns, if a translated label is not needed (e.g.
   if the label is numeric, such as '10-20%'), there is no need to
   fill in any of the translation columns - the original label will
   be used. If, however, a numeric label is edited (e.g. by adding
   a percent sign or other information, even if this doesn't need to
   be translated), it is necessary to copy the relevant text over to
   the translations columns - otherwise the label from the original JSON
   file will be used.
6. For translations, the column name can be anything, as long as it
   starts with 'label_': for clarity, we may want to stick to the
   examples given above, which use the lowercase
   [ISO 639-1 language code](http://en.wikipedia.org/wiki/List_of_ISO_639-1_codes)
   followed by the [ISO 3166-1 country code](http://en.wikipedia.org/wiki/ISO_3166-1).

An example spreadsheet listing all the original English labels can
be found in the file `labels.csv`: this can be opened in most
spreadsheet applications and used as a base for further edits and for
translations.

# Contributors

Andrea Rota <a.rota@lse.ac.uk>

# License

(C) LSE Cities 2015

All code is distributed under the GNU AGPLv3+ license (see LICENSE).
