/**
 * Traverse a SurveyMonkey survey definition JSON file and
 * update (in place) any question or answer label with
 * updated labels provided in a CSV file.
 * 
 * This can be used to update question and answer text using a
 * plain spreadsheet, so that editors don't have to know or worry about
 * the JSON format.
 * 
 * More importantly, it can be used to quickly translate question and
 * answer text across a whole survey by providing alternate language
 * versions in further columns of the spreadsheet.
 * 
 * Syntax:
 * 
 * node update-survey-labels.js --survey-data=<JSON_data_file.json> --text-labels=<text_labels_file.csv> --language-code=<language-code>
 * 
 * The updated/translated JSON file is sent to stdout.
 * 
 * For the spreadsheet format to be used for the text labels file,
 * refer to README.md in this repository.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with this program. If not, see
 * <http://www.gnu.org/licenses/>. 
 */

var traverse = require('traverse');
var fs = require('fs');
var util = require('util');
var parse = require('csv-parse');

var data, language_code;
var argv = require('minimist')(process.argv.slice(2));

/**
 * Parse CLI arguments and emit errors/warnings as applicable if needed
 */
if(!argv['survey-data']) {
  process.stderr.write("Please provide a survey data file (--survey-data=<file.json>).\n");
  process.exit(1);
}

if(!argv['text-labels']) {
  process.stderr.write("Please provide a text labels file (--text-labels=<text.csv>).\n");
  process.exit(1);
}

if(!argv['language-code']) {
  process.stderr.write("No language code provided (e.g. --language-code=<en_GB>): defaulting to en_GB.\n");
  language_code = 'en_GB';
}

language_code = language_code || argv['language-code'];

// Load survey data from JSON file
data = JSON.parse(fs.readFileSync(argv['survey-data'], 'utf8'));

// Load updated labels/translations from CSV file
parse(fs.readFileSync(argv['text-labels'], 'utf8'), {}, function(err, csv_data) {
  var questions, answersm, index_of_wanted_language_column;
  
  index_of_wanted_language_column = csv_data[0].indexOf('label_' + language_code);
  
  // Load labels from CSV file
  questions = labels_from_csv(csv_data, csv_data[0].indexOf('question_id'), index_of_wanted_language_column);
  answers = labels_from_csv(csv_data, csv_data[0].indexOf('answer_id'), index_of_wanted_language_column);

  // Replace labels in JSON data structure with those read from CSV file
  traverse(data).forEach(function(item) {
    if(item !== null && typeof(item)=="object") {
      if(item.answer_id && answers[item.answer_id]) {
        var node = this.node;
        node.text = answers[item.answer_id];
        this.update(node);
      } else if(item.question_id && questions[item.question_id]) {
        var node = this.node;
        node.heading = questions[item.question_id];
        this.update(node);
      }
    }
  });

  console.log(JSON.stringify(data, null, 2));

});

/**
 * Extract { id: text } pairs from CSV file
 * 
 * @param Object csv The csv loader object
 * @param String field The field to look up (question_id or answer_id)
 * @param String language_code The language code to look up for labels
 * @return Object The list of id: text pairs
 */
function labels_from_csv(csv, field, index_of_wanted_language_column) {
  var items = csv.map(function(i) {
    return '' !== i[field] ? [ i[field], i[index_of_wanted_language_column] ] : null;
  });
  
  return items.reduce(function(o, v) {
    if(v !== null && !isNaN(v[0])) {
      o[v[0]] = v[1];
    }
    return o;
  }, {});
}
